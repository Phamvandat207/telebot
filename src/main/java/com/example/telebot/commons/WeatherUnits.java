package com.example.telebot.commons;

/**
 * Weather result units
 */
public enum WeatherUnits {
    metric,
    imperial,
    standard
}
