package com.example.telebot.commons;

public class Constant {

    public static final String APP_CODE_TELEBOT = "LP_CHATBOT";


    public static final String EVENT_TYPE_TET = "TET";
    public static final String EVENT_TYPE_SINH_NHAT = "SINH_NHAT";
    public static final String EVENT_TYPE_THE_THAO = "THE_THAO";
    public static final String EVENT_TYPE_HOP = "CUOC_HOP";
    public static final String EVENT_TYPE_SU_KIEN = "SU_KIEN";
    public static final String EVENT_TYPE_NGHI_TET = "NGHI_TET";
    public static final String EVENT_TYPE_HET_NGHI_TET = "HET_NGHI_TET";


    public static final String EVENT_STATUS_WAITING = "waiting";
    public static final String EVENT_STATUS_CYCLE = "cycle";
    public static final String EVENT_STATUS_DONE = "done";


    public static final String TELEGRAM_API_PARSE_MODE_HTML = "html";



}
