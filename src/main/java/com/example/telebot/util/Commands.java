package com.example.telebot.util;

import lombok.Getter;

public enum Commands {


    START("/start", "start command"),
    HELP("/help", "help command"),
    CREATE("/create", "create command");

    @Getter
    private final String command;
    @Getter
    private final String description;

    Commands(String command, String description){
        this.command = command;
        this.description = description;
    }
}
