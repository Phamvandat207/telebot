package com.example.telebot.util;

import com.example.telebot.model.EmployeeHrisDTO;
import com.example.telebot.model.TemplateDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HttpContext;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Objects;

@Slf4j
@Component
public class IntegrationUtils {

  private static final String GET_TOKEN_ENDPOINT =
      "https://keycloak.lptech.vn/auth/realms/hris/protocol/openid-connect/token";
  private static final String GET_CONTENT_ENDPOINT =
      "https://hris-apis.brostech.xyz/templates/render";
  private static final String GET_EMPLOYEES_BIRTHDAY_NOW =
      "https://hris-apis.brostech.xyz/employees/notification-birthdate";

  public static String getToken() throws JsonProcessingException {
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
    map.add("grant_type", "password");
    map.add("client_secret", "2fede3b9-f307-4ef6-9f6e-34941c685ca1");
    map.add("client_id", "services");
    map.add("username", "employee1");
    map.add("password", "123123");
    HttpEntity<MultiValueMap<String, String>> request =
        new HttpEntity<MultiValueMap<String, String>>(map, headers);
    ResponseEntity<String> response =
        restTemplate.postForEntity(GET_TOKEN_ENDPOINT, request, String.class);
    if (response.getStatusCode().is2xxSuccessful()) {
      ObjectMapper mapper = new ObjectMapper();
      JsonNode token = mapper.readTree(response.getBody());
      return token.get("access_token").toString();
    }
    return null;
  }

  public static String getContent(TemplateDTO templateDTO) throws JsonProcessingException {
    log.info("Start call API Get Content by TemplateCode");
    CloseableHttpClient httpClient =
        HttpClientBuilder.create()
            .setRedirectStrategy(
                new DefaultRedirectStrategy() {

                  @Override
                  public boolean isRedirected(
                      HttpRequest request, HttpResponse response, HttpContext context)
                      throws ProtocolException {

                    System.out.println(response);
                    // If redirect intercept intermediate response.
                    if (super.isRedirected(request, response, context)) {
                      int statusCode = response.getStatusLine().getStatusCode();
                      String redirectURL = response.getFirstHeader("Location").getValue();
                      System.out.println("redirectURL: " + redirectURL);
                      return true;
                    }
                    return false;
                  }
                })
            .build();
    HttpComponentsClientHttpRequestFactory factory =
        new HttpComponentsClientHttpRequestFactory(httpClient);

    RestTemplate restTemplate = new RestTemplate(factory);
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.add("Authorization", "Bearer " + Objects.requireNonNull(getToken()).replace("\"", ""));
    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
    map.add("applicationCode", "MC_HRIS");
    map.add("templateCode", "MC_CONTRACT_TEMPLATE_01");
    //        map.add("model_input", obj.toString());
    ObjectMapper objectMapper = new ObjectMapper();
    HttpEntity<String> request =
        new HttpEntity<String>(objectMapper.writeValueAsString(templateDTO), headers);
    ResponseEntity<String> response =
        restTemplate.postForEntity(GET_CONTENT_ENDPOINT, request, String.class);
    if (response.getStatusCode().is2xxSuccessful()) {
      return response.getBody();
    }
    return null;
  }

  public static EmployeeHrisDTO[] getAllEmployee()
      throws URISyntaxException, JsonProcessingException {
    var now = LocalDate.now();
    HttpHeaders headers = new HttpHeaders();
    headers.add("Authorization", "Bearer " + Objects.requireNonNull(getToken()).replace("\"", ""));
    RestTemplate restJobPosition = new RestTemplate();
    ResponseEntity<EmployeeHrisDTO[]> listEmployeeReturn =
        restJobPosition.exchange(
            RequestEntity.get(new URI(GET_EMPLOYEES_BIRTHDAY_NOW + "?birthDate=" + now))
                .headers(headers)
                .build(),
            EmployeeHrisDTO[].class);

    return listEmployeeReturn.getBody();
  }
}
