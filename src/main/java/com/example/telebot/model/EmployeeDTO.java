package com.example.telebot.model;

import com.example.telebot.domain.Employee;
import lombok.*;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class EmployeeDTO {
    private int id;
    private String name;
    private LocalDate dob;
    private String image;
    private String birthday;

    public EmployeeDTO(Employee employee) {
        this.id = employee.getId();
        this.name = employee.getName();
        this.dob = employee.getDob();
        this.image = employee.getImage();

        if (this.dob != null) {
            this.birthday = String.valueOf(this.dob.getDayOfMonth() + this.dob.getMonthValue());
        }
    }
}
