package com.example.telebot.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class TemplateDTO {
    private String applicationCode;
    private String templateCode;
    private Object modelInput;

}