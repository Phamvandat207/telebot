package com.example.telebot.model;

import com.example.telebot.domain.Event;
import lombok.*;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class EventDTO {
    private int id;
    private String name;
    private String type;
    private LocalDateTime scheduler;
    private String templateCode;
    private String status;

    public EventDTO(Event event){
        this.id = event.getId();
        this.name = event.getName();
        this.type = event.getType();
        this.scheduler = event.getScheduler();
        this.templateCode = event.getTemplateCode();
        this.status = event.getStatus();
    }
}