package com.example.telebot.job;

import lombok.extern.slf4j.Slf4j;

import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.Duration;

@Slf4j
public class Weather {
    private static final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .connectTimeout(Duration.ofSeconds(10))
            .build();


    public String getCurrentWeather(String cityName) {
        if (cityName != null && !cityName.isBlank()) {
            try {
                String urlString = "https://vi.wttr.in/" + URLEncoder.encode(cityName, StandardCharsets.UTF_8) + "?m?T?tqp0";
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(URI.create(urlString))
                        .GET()
                        .build();
                HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
                String body = response.body();
                return  "<code><b> Thời tiết: " + body.substring(body.indexOf("<pre>") + 5, body.indexOf("</pre>")).trim() + "</b></code>";
            }
            catch (Exception ex) {
                log.error("Có lỗi xảy ra, thời tiết", ex);
                return "";
            }
        }
        else {
            log.error("Tên thành phố trống, thời tiết");
            return "";
        }
    }
}
