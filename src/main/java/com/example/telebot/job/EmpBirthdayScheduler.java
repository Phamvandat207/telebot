package com.example.telebot.job;

import com.example.telebot.bot.telegram.SendMsgBot;
import com.example.telebot.commons.Constant;
import com.example.telebot.model.EmployeeDTO;
import com.example.telebot.model.EmployeeHrisDTO;
import com.example.telebot.model.TemplateDTO;
import com.example.telebot.service.EmployeeService;
import com.example.telebot.util.IntegrationUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

@Slf4j
@Component
public class EmpBirthdayScheduler {

    @Autowired
    private EmployeeService employeeService;

    @Value("${app.config.telegram.app-id}")
    private String appId;

    @Transactional
    @Scheduled(cron = "0 45 8 ? * *")
    @Scheduled(fixedDelay = 180000)
    public void scheduleTaskWithCronExpression() throws URISyntaxException, JsonProcessingException {
        log.info("Start");
        var now = LocalDate.now();
        String dateMatch = "" + now.getDayOfMonth() + now.getMonthValue();
        log.info(":: EmpBirthdayScheduler > scheduleTaskWithCronExpression > dateMatch[{}] ", dateMatch);
        SendMsgBot bot = null;
        String content = null;
        TemplateDTO templateDTO = null;
        List<EmployeeDTO> birthdays = employeeService.findAllByBirthday(dateMatch);

        var employeelist = IntegrationUtils.getAllEmployee();
        Arrays.stream(employeelist).forEach(x -> birthdays.add(new EmployeeDTO(1, x.getFullName(), null, null, null)));

        EmployeeDTO res = null;
        for (EmployeeDTO emp : birthdays) {
            bot = new SendMsgBot();
            templateDTO = new TemplateDTO();
            templateDTO.setApplicationCode(Constant.APP_CODE_TELEBOT);
            String templateCode_BIRTHDAY = "BIRTH_TEMP_01";
            templateDTO.setTemplateCode(templateCode_BIRTHDAY);
            res = new EmployeeDTO();
            res.setName(emp.getName());
            templateDTO.setModelInput(res);
            try {
                content = IntegrationUtils.getContent(templateDTO);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                log.info(":: EmpBirthdayScheduler > scheduleTaskWithCronExpression > exception: {}", e.toString());
            }
            if (StringUtils.isBlank(content)) {
                continue;
            }
//            log.info(content);
            bot.sendMsg(appId, content, Constant.TELEGRAM_API_PARSE_MODE_HTML);

        }
    }
}
