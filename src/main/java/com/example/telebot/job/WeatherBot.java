package com.example.telebot.job;

import com.example.telebot.bot.telegram.SendMsgBot;
import com.example.telebot.commons.WeatherUnits;
import com.example.telebot.util.JsonUtil;
import com.example.telebot.weatherTypes.CurrentWeather;
import com.example.telebot.weatherTypes.Forecast;
import com.example.telebot.weatherTypes.WeatherCondition;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/** Weather class */
@Slf4j
@Component
public class WeatherBot {

  @Value("${bot.open_weather_map_api_key}")
  private String weatherApiKey = "";

  private String language = "vi";
  private WeatherUnits units = WeatherUnits.metric;

  private final String weatherApiUrl = "http://api.openweathermap.org/data/2.5/{endpoint}";

  @Value("${app.config.telegram.app-id}")
  private String appId;

  private final int TEMPERATURE_LIMIT = 15;
  private boolean WARNING_RAIN = false;
  private boolean WARNING_TEMPERATURE = false;

//  @Scheduled(cron = "0 0 7 ? * *")
  @Scheduled(fixedDelay = 180000)
  private void notifyForecast() {
    String city = "hanoi";
    log.info(":: WeatherBot > notifyForecast : Start");
    try {
      Forecast forecast = getForecast(city);

      List<String> mainWeatherConditions = new ArrayList<>();

      LocalDate now = LocalDate.now();
      int minTempDay = Integer.MAX_VALUE;
      int maxTemDay = Integer.MIN_VALUE;
      log.info(":: WeatherBot > notifyForecast : forecast.getList().size[{}]", forecast.getList().size());
      for (CurrentWeather weather : forecast.getList()) { // cần xử lý dữ liệu thời gian trong ngày
        if(Arrays.stream(weather.getDateText().split(" ")).findFirst().get().equals(now.toString())){
          int minTempHour = (int) Math.round(weather.getWeather().getMinTemperature());
          int maxTempHour = (int) Math.round(weather.getWeather().getMaxTemperature());
          String weatherConditions = weather.getWeatherCondition().get(0).getMain();
          mainWeatherConditions.add(weatherConditions);
          if (minTempHour < minTempDay) minTempDay = minTempHour;
          if (maxTempHour > maxTemDay) maxTemDay = maxTempHour;
        }
      }
      String cityForecast = forecast.getCity().getName();
      String countryForecast = forecast.getCity().getCountry();
      if(!hasNotifyForecast(mainWeatherConditions, minTempDay)){
        return;
      }
      String forecastStr =
          getContentForecast(
              mainWeatherConditions, minTempDay, maxTemDay, cityForecast, countryForecast, now);

      log.info(":: WeatherBot > sendForecast > forecastStr : [{}] ", forecastStr);
      sendReply(forecastStr);
    } catch (Exception e) {
      log.error("Forecast error", e);
      sendReply("forecast_get_error");
    }
  }

  private boolean hasNotifyForecast(List<String> mainWeatherConditions, int minTempDay) {
    WARNING_RAIN = false;
    WARNING_RAIN = false;
    String[] rains = {"Thunderstorm", "Drizzle", "Rain", "Rain"};
    for (String rain : rains) {
      if (mainWeatherConditions.contains(rain)) {
        WARNING_RAIN = true;
        break;
      }
    }
    if (minTempDay < TEMPERATURE_LIMIT) {
      WARNING_RAIN = true;
    }
    return (WARNING_TEMPERATURE || WARNING_RAIN);
  }

  private String getContentForecast(
      List<String> mainWeatherConditions,
      int minTempDay,
      int maxTemDay,
      String cityForecast,
      String countryForecast,
      LocalDate date) {
    DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    String dateStr = date.format(formatDate);
    String contentReturn = "";
    contentReturn += String.format("Dự báo thời tiết:  %s, %s\n", cityForecast, countryForecast);
    contentReturn += String.format("Ngày      : %s\n", dateStr);
    contentReturn += String.format("Nhiệt độ  : %d - %d °C\n", minTempDay, maxTemDay);

    String[] rains = {"Thunderstorm", "Drizzle", "Rain", "Rain"};
    for (String rain : rains) {
      if (mainWeatherConditions.contains(rain)) {
        WARNING_RAIN = true;
        break;
      }
    }
    if (minTempDay < TEMPERATURE_LIMIT) {
      WARNING_TEMPERATURE = true;
    }
    // Thêm thông tin nhắc nhở mưa hoặc nhiệt độ thấp
    if (WARNING_RAIN && WARNING_TEMPERATURE) {
      contentReturn +=
          "Lưu ý     : Trời lạnh, có mưa.\n\nMọi người nên mặc ấm và mang thêm ô dù, áo mưa khi đi làm nhé.\nChúc cả nhà ngày mới vui vẻ.";
    } else if (WARNING_RAIN) {
      contentReturn +=
          "Lưu ý     : Có mưa.\n\nLPTech_Bot nhắc nhở mọi người nên đem theo ô dù, áo mưa khi ra ngoài nhé.\nChúc cả nhà ngày mới vui vẻ.";
    } else if (WARNING_TEMPERATURE) {
      contentReturn +=
          "Lưu ý     : Nhiệt độ xuống thấp.\n\nLPTech_Bot nhắc nhở mọi người mặc ấm, giữ gìn sức khởe thật tốt.\nChúc cả nhà một ngày làm việc hiệu quả.";
    }
    return "<code>" + contentReturn + "</code>";
  }

  ///////////////////////////////////////////////////////////////////////////

  /**
   * Get current weather
   *
   * @param city City to get
   * @return Current weather
   * @throws Exception
   */
  private CurrentWeather getCurrentWeather(String city) throws Exception {
    try {
      JSONObject weatherObject = getWeatherObject("weather", city);
      CurrentWeather weather = JsonUtil.toObject(weatherObject, CurrentWeather.class);
      if (weather == null) {
        throw new Exception("Cannot parse weather");
      }
      return weather;
    } catch (Exception e) {
      log.error("Cannot get weather data", e);
      throw e;
    }
  }

  /**
   * Get 5 day forecast
   *
   * @param city City to get
   * @return Weather forecast
   * @throws Exception
   */
  private Forecast getForecast(String city) throws Exception {
    try {
      JSONObject forecastObject = getWeatherObject("forecast", city);
      Forecast forecast = JsonUtil.toObject(forecastObject, Forecast.class);
      if (forecast == null) {
        throw new Exception("Cannot parse forecast");
      }
      return forecast;
    } catch (Exception e) {
      log.error("Cannot get forecast", e);
      throw e;
    }
  }

  ///////////////////////////////////////////////////////////////////////////

  /**
   * Get weather object
   *
   * @param endpoint API endpoint
   * @param city City to get
   * @return Weather json object
   */
  private JSONObject getWeatherObject(String endpoint, String city) throws Exception {
    HttpResponse<JsonNode> response =
        Unirest.get(weatherApiUrl)
            .routeParam("endpoint", endpoint)
            .queryString("APPID", weatherApiKey)
            .queryString("lang", language)
            .queryString("units", units.toString())
            .queryString("q", city)
            .asJson();
    return response.getBody().getObject();
  }

  /**
   * Send reply message to user without throwing an exception
   *
   * @param message Message
   */
  private void sendReply(String message) {
    try {
      SendMsgBot bot = new SendMsgBot();
      bot.sendMsg(appId, message, "html");
    } catch (Exception e) {
      log.error("Cannot send reply to " + appId, e);
    }
  }

  ///////////////////////////////////////////////////////////////////////////

  /**
   * Collect conditions string
   *
   * @param conditions Conditions list
   * @return Collected conditions
   */
  private String getConditions(List<WeatherCondition> conditions) {
    return conditions.stream()
        .map(WeatherCondition::getDescription)
        .collect(Collectors.joining(", "));
  }

  ///////////////////////////////////////////////////////////////////////////

  public String getWeatherApiKey() {
    return weatherApiKey;
  }

  public String getWeatherApiUrl() {
    return weatherApiUrl;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
    //    messages = ResourceBundle.getBundle(messagesBundleName, new Locale(language));
  }

  public WeatherUnits getUnits() {
    return units;
  }

  public void setUnits(WeatherUnits units) {
    this.units = units;
  }
}
