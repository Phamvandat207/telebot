package com.example.telebot.job;

import com.example.telebot.bot.telegram.SendMsgBot;
import com.example.telebot.commons.Constant;
import com.example.telebot.domain.Employee;
import com.example.telebot.domain.Event;
import com.example.telebot.model.EmployeeDTO;
import com.example.telebot.model.TemplateDTO;
import com.example.telebot.service.EmployeeService;
import com.example.telebot.service.EventService;
import com.example.telebot.util.IntegrationUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.telegram.telegrambots.meta.api.objects.InputFile;

import javax.transaction.Transactional;
import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Component
public class ScheduledTasks {
  final EventService eventService;

  final EmployeeService employeeService;

  @Value("${app.config.telegram.app-id}")
  private String appId;

  @Value("${app.config.telegram.bot-token}")
  private String botToken;

  public ScheduledTasks(EventService eventService, EmployeeService employeeService) {
    this.eventService = eventService;
    this.employeeService = employeeService;
  }

  /** Lên lịch quét event sau 30ph */
  @Transactional
  @Scheduled(fixedRate = 1800000)
  public void generalEvent() throws JsonProcessingException {
    log.info("Start generalEvent");
    List<Event> events = eventService.findEvent();
    log.info(":: ScheduleTask > notifyEvent > appID(groupID): {} ", appId);
//    log.info(":: ScheduleTask > notifyEvent > datetime: {} ", LocalDateTime.now());
    SendMsgBot bot = null;
    String content = null;
    String folder = "default";
    int random = 1;
    List<EmployeeDTO> birthdays = null;
    TemplateDTO templateDTO = null;
    var now = LocalDate.now();
    String dateMatch = null;
    Employee res = null;
    for (Event item : events) {
      bot = new SendMsgBot();
      templateDTO = new TemplateDTO();
      templateDTO.setApplicationCode(Constant.APP_CODE_TELEBOT);

      if (item.getType().equals(Constant.EVENT_TYPE_TET)) {
        templateDTO.setTemplateCode(item.getTemplateCode());
        folder = "new-year";
        content = IntegrationUtils.getContent(templateDTO);
        if (StringUtils.isBlank(content)) {
          continue;
        }
        // random animation
        bot.sendPhoto(
            appId,
            new InputFile(new File("src/main/resources/img/" + folder + "/" + random + ".png")));
        bot.sendMsg(appId, content, Constant.TELEGRAM_API_PARSE_MODE_HTML);
        item.setStatus(Constant.EVENT_STATUS_DONE);
        eventService.updateEvent(item);
      } else if (item.getType().equals(Constant.EVENT_TYPE_NGHI_TET)
          || item.getType().equals(Constant.EVENT_TYPE_HET_NGHI_TET)) {
        templateDTO.setTemplateCode(item.getTemplateCode());
        content = IntegrationUtils.getContent(templateDTO);
        if (StringUtils.isBlank(content)) {
          continue;
        }
        bot.sendMsg(appId, content, Constant.TELEGRAM_API_PARSE_MODE_HTML);
        item.setStatus(Constant.EVENT_STATUS_DONE);
        eventService.updateEvent(item);
      } else if (item.getType().equals(Constant.EVENT_TYPE_SINH_NHAT)) {
        folder = "birthday";
        //                dateMatch = "" + (now.getDayOfMonth()-1) + now.getMonthValue();
        dateMatch = "271";
        log.info(
            ":: ScheduleTask > notifyEvent > eventType[SINH_NHAT] > dateMatch[{}] ", dateMatch);
        templateDTO.setTemplateCode(item.getTemplateCode());
        birthdays = employeeService.findAllByBirthday(dateMatch);
        if (CollectionUtils.isEmpty(birthdays)) {
          continue;
        }
        for (EmployeeDTO employee : birthdays) {
          res = new Employee();
          res.setName(employee.getName());
          templateDTO.setModelInput(res);
          content = IntegrationUtils.getContent(templateDTO);
          if (StringUtils.isBlank(content)) {
            continue;
          }
          log.info(content);
          bot.sendAnimation(
              appId,
              new InputFile(new File("src/main/resources/img/" + folder + "/" + random + ".gif")));
          bot.sendMsg(appId, content, Constant.TELEGRAM_API_PARSE_MODE_HTML);
        }
      }
    }
  }
}
