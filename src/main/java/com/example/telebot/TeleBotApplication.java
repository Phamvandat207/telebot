package com.example.telebot;

import com.example.telebot.bot.telegram.SendMsgBot;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@SpringBootApplication
@EnableScheduling
public class TeleBotApplication {
  public static void main(String[] args) {
    SpringApplication.run(TeleBotApplication.class, args);
    try {
      TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
      telegramBotsApi.registerBot(new SendMsgBot());
    } catch (TelegramApiException e) {
      e.printStackTrace();
    }
  }
}
