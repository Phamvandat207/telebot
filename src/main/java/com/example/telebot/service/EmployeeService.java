package com.example.telebot.service;

import com.example.telebot.domain.Employee;
import com.example.telebot.model.EmployeeDTO;

import java.time.LocalDate;
import java.util.List;

public interface EmployeeService {
    List<Employee> findAll();

    List<Employee> findAllByDob(LocalDate date);

    Employee findById(int id);

    Employee createEmployee(Employee employee);

    Employee updateEmployee(Employee employee);

    void deleteEmployee(int id);

    List<EmployeeDTO> findAllByBirthday(String birthday);
}
