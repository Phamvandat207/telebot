package com.example.telebot.service.impl;

import com.example.telebot.commons.Constant;
import com.example.telebot.domain.Event;
import com.example.telebot.repository.EventRepository;
import com.example.telebot.service.EventService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class EventServiceImpl implements EventService {
    @Autowired
    EventRepository eventRepository;

    @Override
    public List<Event> findAll() {
        return eventRepository.findAll();
    }

    @Override
    public Optional<List<Event>> findByDay(LocalDate date) {
        return eventRepository.findByScheduler(date);
    }

    @Override
    public Event createEvent(Event event) {
        if (eventRepository.existsByIdOrName(event.getId(), event.getName())) {
            return null;
        }
        eventRepository.save(event);
        return event;
    }

    @Override
    public Event updateEvent(Event event) {
        if (eventRepository.existsById(event.getId())) {
            Event current = eventRepository.getById(event.getId());
            if (eventRepository.existsByName(event.getName()) && event.getName().equals(current.getName())) {
                return null;
            } else {
                current.setName(event.getName());
                eventRepository.save(current);
                return current;
            }
        }
        return null;
    }

    @Override
    public void deleteEvent(int id) throws FileNotFoundException {
        if (!eventRepository.existsById(id)) {
            throw new FileNotFoundException("Không tìm thấy dữ liệu");
        }
        eventRepository.deleteById(id);
    }

    @Override
    public List<Event> findEvent() {
        var now = LocalDateTime.now();
        var startTime = now.minusMinutes(30);
        var endTime = now.plusMinutes(30);
        List<String> status = new ArrayList<>();
        status.add(Constant.EVENT_STATUS_WAITING);
        status.add(Constant.EVENT_STATUS_CYCLE);
        try {
            List<Event> events = eventRepository.findAllBySchedulerBetweenAndStatusIn(startTime, endTime, status);
            log.info(":: EventServiceImpl > findEvent > events size[{}]", events.size());
            return events;
        } catch (Exception e) {
            log.info(":: EventServiceImpl > findEvent > exception {}", e.getMessage());
            return null;
        }

    }
}