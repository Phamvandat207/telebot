package com.example.telebot.service.impl;

import com.example.telebot.domain.Employee;
import com.example.telebot.model.EmployeeDTO;
import com.example.telebot.repository.EmployeeRepository;
import com.example.telebot.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public List<Employee> findAll() {
        return new ArrayList<>(employeeRepository.findAll());
    }

    @Override
    public List<Employee> findAllByDob(LocalDate date) {
        Optional<List<Employee>> listEmployeeByDob = employeeRepository.findAllByDob(date);
        return listEmployeeByDob.orElse(null);
    }

    @Override
    public Employee findById(int id) {
        Optional<Employee> employee = employeeRepository.findById(id);
        return employee.orElse(null);
    }

    @Override
    public Employee createEmployee(Employee employee) {
        if (employeeRepository.existsById(employee.getId())) {
            return null;
        }
        employeeRepository.save(employee);
        return employee;
    }

    @Override
    public Employee updateEmployee(Employee newEmployeeData) {
        Employee targetEmployee = employeeRepository.getById(newEmployeeData.getId());
        if (employeeRepository.existsById(newEmployeeData.getId())) {
            targetEmployee.setName(newEmployeeData.getName());
            targetEmployee.setDob(newEmployeeData.getDob());
            targetEmployee.setImage(newEmployeeData.getImage());
            employeeRepository.save(targetEmployee);
            return targetEmployee;
        }
        return null;
    }

    @Override
    public void deleteEmployee(int id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public List<EmployeeDTO> findAllByBirthday(String birthday) {
        List<Employee> employees = employeeRepository.findAllByBirthday(birthday);
        log.info(":: EmployeeServiceImpl > findAllByBirthday > employeesSize[{}]", employees.size());
        List<EmployeeDTO> employeeDTOs = new ArrayList<>();
        if(CollectionUtils.isEmpty(employees)){
            return employeeDTOs;
        }
        for (Employee em : employees) {
            employeeDTOs.add(new EmployeeDTO(em));
        }
        return employeeDTOs;
    }
}
