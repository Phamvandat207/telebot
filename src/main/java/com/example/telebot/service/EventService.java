package com.example.telebot.service;

import com.example.telebot.domain.Event;

import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface EventService {
    List<Event> findAll();

    Optional<List<Event>> findByDay(LocalDate date);

    Event createEvent(Event event);

    Event updateEvent(Event event);

    void deleteEvent(int id) throws FileNotFoundException;

    List<Event> findEvent();
}
