package com.example.telebot.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Date;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "dob")
    private LocalDate dob;

    @Column(name = "image")
    private String image;

    @Column(name = "birthday")
    private String birthday;

    public String getBirthday() {
        if (this.dob != null) {
            return String.valueOf(this.dob.getDayOfMonth() + this.dob.getMonthValue());
        }
        return null;
    }
}
