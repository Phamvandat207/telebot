package com.example.telebot.repository;

import com.example.telebot.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    Optional<List<Employee>> findAllByDob(LocalDate date);

    List<Employee> findAllByBirthday(String birthday);
}
