package com.example.telebot.repository;

import com.example.telebot.domain.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {
    Optional<List<Event>> findByScheduler(LocalDate date);

    Boolean existsByIdOrName(int id, String name);

    Boolean existsById(int id);

    Boolean existsByName(String name);

    List<Event> findAllBySchedulerBetweenAndStatusIn( LocalDateTime startDate, LocalDateTime endDate, List<String> status);

}
